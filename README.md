# askiiart/dotfiles

My dotfiles for:

- fish
  - other shells in the future maybe?
- kitty
- nvim
- gpg
- Vencord Desktop (Vesktop)
- ~~sway~~ [SwayFX](https://github.com/WillPower3309/swayfx)
- VS Code
- Claws Mail
- Rofi
- GnuPG
- Git
- WezTerm
- greetd
- fonts
- Waybar
- and whatever other stuff i end up adding but forget to put in this list

NOTE: Sway uses swayfx now, which has a bunch of effects

---

![A screenshot of fastfetch (like neofetch) running with these dotfiles](/screenshot.png)

---

- NOTE: `librewolf/prefs.js` MUST end in a newline for `read` to work
  - [userChrome.css stolen from aagaming](https://git.catvibers.me/aa/nix/src/commit/42c4ee8d52538ee5f53045a90f528072e12c097c/desktop/apps/web/userChrome.css)

---

TODO:

- figure out how to script firefox/librewolf extension installation
- maybe just rewrite it in rust or something entirely??? idk
- fix swaylock background generation
